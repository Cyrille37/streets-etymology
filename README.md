# Étymologie des rues d'une commune

Les noms de rues, appellé aussi odonyme ou hodonyme.

Il existe des `tags` pour donner une sémantique à des objets OpenStreetMap, en faisant une liaison avec Wikidata et Wikipedia.

Concernant les rues ce lien sémantique permet de retrouver à partir de leur nom:
- personne (human Q5)
  - genre (sex or gender P21)
    - female Q6581072
    - male Q6581097
  - activité (principale)
    - artistes: pöete, écrivain, sculpteur, peintre...
    - scientifique: biologiste, mathématicien, physicien...
    - aviateur
- autre
  - bataille
  - ville, pays (P131)
    - municipality section Q2785216, subdivisions of Belgium Q15623264 ...
    - human settlement Q486972, populated place Q123964505, artificial geographic entity Q27096235
  - plantes

Ce bout de code permet d'extraire les rues d'une ville pour les traiter avec un tableur ou mieux avec OpenRefine, puis importer le résultats dans OpenStreetMap.

## Crédits

![Logo OpenStreetMap](https://upload.wikimedia.org/wikipedia/commons/b/b0/Openstreetmap_logo.svg "Utilise des données provenant de OpenStreetMap"){height=80px}
![Logo Wikidata Powered](https://upload.wikimedia.org/wikipedia/commons/4/41/Wikidata_Stamp_Rec_Light.svg "Utilise des données provenant de Wikidata"){width=120px}
![Logo Framasoft](https://upload.wikimedia.org/wikipedia/commons/b/bb/Framasoft_Logo.svg "Ce code source est hébergé et partagé grâce à Framasoft"){height=80px}

## Utilisation du code

### Partie commune

les scripts utilisent la librairie [Cyrille37\OSM\Yapafo\OSM_Api](https://github.com/Cyrille37/yapafo) qui peut être configurée avec un fichier `.env` que l'on obitient en copiant le fichier `.env.example`. Pour un usage simple et en lecture seule la configuration par défaut est suffisante.

Pour le développement (modification des scripts) la configuration permet notamment de définir des caches pour éviter de surchager les services par de trop nombreuses requêtes (essais/erreurs).

Pour mettre à jour la base de données OpenStreetMap il faut configurer les données d'authentification en suivant les indications du fichier [Yapafo/README.md](vendor/cyrille37/yapafo/README.md).

### script osm-city-streets-extract.php

Récupère les rues et autres passages d'une commune et créer un fichier `CSV`.

L'exécution du script sans paramètre pose les questions nécessaires à son fonctionnement.

Les instructions plus détaillées sont au début du fichier [osm-city-streets-extract.php](osm-city-streets-extract.php).

## Overpass

- https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL
- https://wiki.openstreetmap.org/wiki/User:Cyrille37/En_vrac#%C3%89TYMOLOGIE

Export CSV des rues de la ville.

export OverPass en CSV

```overpass
// @name export des rues d’une ville en CSV
{{country="France"}}
{{ville="Tours"}}
[out:csv(::type, ::id, name, wikidata, "name:etymology:wikidata", wikipedia, "name:etymology:wikipedia" ; true; ",")];
area[admin_level=2][name={{country}}]->.country;
area[admin_level=8][name={{ville}}](area.country)->.ville;
(
  way(area.ville)[highway][highway!~"^platform$"][name];
  relation(area.ville)[type~"^associatedStreet|street$"];
);
out body;
```

Puis refaire un `tri des données` dans un tableur.

## Wikidata

Wikidata properties
- [nature de l’élément](https://www.wikidata.org/wiki/Property:P31)
  - [commune française](https://www.wikidata.org/wiki/Q484170)
    - nature de l’élément [type de division administrative en France](https://www.wikidata.org/wiki/Q20667921)
      - [sous-classe de](https://www.wikidata.org/wiki/Property:P279)
        - [type de division administrative](https://www.wikidata.org/wiki/Q15617994)

Pour trouver les liens ou liens manquants entre OSM et Wikidata
  - https://map.osm.wikidata.link/item/Q118531043
Présentations Wikidata + OpenStreetMap
  - Naveen Francis Digital Humanitarian https://wiki.openstreetmap.org/w/images/0/0a/Wikidata%2BOpenStreetMap.pdf

Étymologie dans OSM:
- Une [visualisation interactive](https://etymology.dsantini.it/?lang=fr#0.6823,47.4176,15.2,blue,pmtiles_all,stamen_toner) avec le projet [Danysan1/open-etymology-map](https://github.com/Danysan1/open-etymology-map) basé sur [openetymologymap/osm-wikidata-map-framework](https://gitlab.com/openetymologymap/osm-wikidata-map-framework).
  - Avec la question [name:etymology:wikidata on associatedStreet relation?](https://github.com/Danysan1/open-etymology-map/issues/4)

Question: What is the "main type" of an entities list
- https://www.wikidata.org/wiki/Wikidata:Request_a_query#What_is_the_%22main_type%22_of_an_entities_list

[Comment trouvé les oiseaux parmi les êtres vivants ?](https://www.wikidata.org/wiki/Wikidata:Bistro#Comment_trouv%C3%A9_les_oiseaux_parmi_les_%C3%AAtres_vivants_%3F)

Outils d'exploration
- Wikidata API
  - https://www.wikidata.org/w/api.php
  - https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels|claims&ids=Q19675&languages=fr&format=json
- Wikidata Graph Builder
  - https://angryloki.github.io/wikidata-graph-builder/?item=Q25234&property=P171&iterations=30&lang=fr
- Wikidata Query Builder
  - https://query.wikidata.org/querybuilder/?uselang=fr
- Wikidata Query service
  - [Merle Noir -> Oiseau](https://query.wikidata.org/#%23defaultView%3AGraph%0APREFIX%20gas%3A%20%3Chttp%3A%2F%2Fwww.bigdata.com%2Frdf%2Fgas%23%3E%0A%0ASELECT%20%3Fitem%20%3FitemLabel%20%3FlinkTo%20%7B%0ASERVICE%20gas%3Aservice%20%7B%0A%20%20%20%20gas%3Aprogram%20gas%3AgasClass%20%22com.bigdata.rdf.graph.analytics.SSSP%22%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20gas%3Ain%20wd%3AQ25234%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20gas%3AtraversalDirection%20%22Forward%22%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20gas%3Aout%20%3Fitem%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20gas%3Aout1%20%3Fdepth%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20gas%3AmaxIterations%2030%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20gas%3AlinkType%20wdt%3AP171%20.%0A%20%20%7D%0AOPTIONAL%20%7B%20%3Fitem%20wdt%3AP171%20%3FlinkTo%20%7D%0ASERVICE%20wikibase%3Alabel%20%7Bbd%3AserviceParam%20wikibase%3Alanguage%20%22fr%2Cen%22%20%7D%0A%7D)

```SPARQL
ASK {
  # Moby Dick est-il une oeuvre créative ?
  #wd:Q174596 wdt:P31/wdt:P279* wd:Q17537576.
  
  # Honoré de Balzac est-il un écrivain ?
  #wd:Q9711 wdt:P106*/wdt:P101* wd:Q36180.
 
  # orfèvre est-il une activité ?
  #wd:Q211423 wdt:P31/wdt:P279* wd:Q12737077.

  # le cygne est-il un oiseau ?
  #wd:Q29564928 wdt:P171* wd:Q5113.

  # optimization
  hint:Prior hint:gearing "forward".
}
```

[RDF GAS API](https://github.com/blazegraph/database/wiki/RDF_GAS_API)

```SPARQL
# https://query.wikidata.org/
#defaultView:Graph
PREFIX gas: <http://www.bigdata.com/rdf/gas#>

SELECT ?item ?itemLabel ?linkTo {
SERVICE gas:service {
    gas:program gas:gasClass "com.bigdata.rdf.graph.analytics.SSSP" ;
                gas:in wd:Q9482 ; # écureuil
                #gas:target wd:Q5113 ;
                gas:traversalDirection "Forward" ;
                gas:out ?item ;
                gas:out1 ?depth ;
                gas:maxIterations 20 ;
                gas:linkType wdt:P171 ; # Taxon supérieur
  }
OPTIONAL { ?item wdt:P171 ?linkTo }
SERVICE wikibase:label {bd:serviceParam wikibase:language "fr,en" }
}
```

### Tech

- https://www.wikidata.org/w/api.php?

Récupérer les labels (libellés) & claims (déclarations)

https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels|claims&ids=Q19675&languages=fr&format=json

## OpenRefine

Add column based on column X

Grel expression
```
value
	.match(/(Allée de |Allée du |Allée d\'|Allée |Rue de |Rue du |Rue d\'|Rue |Avenue de |Avenue du |Avenue d\'|Avenue |Boulevard de |Boulevard d\'|Boulevard )(.*)/)
	[1]
```

Transform

```
if( isNotNull(value),
 value,
 cells['name'].value.match(/(Impasse de |Impasse du |Impasse d\'|Impasse |Jardin du |Mail de |Mail du |Mail |Passage de |Passage du |Passage d\'|Passage |Place du |Place des |Place de |Place d\'|Place |Quai de |Quai du |Route de |Route du)(.*)/)[1]
)
```

## Recherche historique

- BNF https://catalogue.bnf.fr/
  - https://gallica.bnf.fr
- Geneanet https://gw.geneanet.org/
- Base Légion d'honneur https://www.leonore.archives-nationales.culture.gouv.fr
- France Archives https://francearchives.gouv.fr/
- Google books https://www.google.fr/books/
- Base biographique Université Paris Cité
  - https://www.biusante.parisdescartes.fr/histoire/biographies/index.php?cle=14728
-  Comité des travaux historiques et scientifiques, Institut rattaché à l’École nationale des chartes 
  - https://cths.fr/an/societe.php?id=345

### pour la ville de Tours

Le livre ["Tours pas à pas : ses rues, ses monuments, ses hommes célèbres"](https://www.google.fr/books/edition/Tours_pas_%C3%A0_pas_ses_rues_ses_monuments/k-vvDwAAQBAJ?hl=fr&gbpv=0)
Par Hélène Vialles, Horvath, Le Coteau, 1985
- https://catalogue.bnf.fr/ark:/12148/cb34913879w
- https://www.placedeslibraires.fr/ebook/9782402087087-tours-pas-a-pas-ses-rues-ses-monuments-ses-hommes-celebres-helene-vialles/

Le livre ["À la découverte des noms des rues de Tours"](https://www.google.fr/books/edition/%C3%80_la_d%C3%A9couverte_des_noms_des_rues_de_T/vEcqAQAACAAJ?hl=fr)
Par Geneviève Gascuel, Éditions CMD, Montreuil-Bellay, 1999
- https://catalogue.bnf.fr/ark:/12148/cb37120105x

Le livre ["Aux noms de Tours"](https://editions-incunables.fr/catalogue-papier/)
Aux noms de Tours / Ecole ESTEN, Emmanuel Roc. Tours Editions Incunables 2.0 2012
Année 2011-2012, 124 pages
https://recherches-archives.tours.fr/archives/show/FRAC037261_IR000000210_de-239

Archives municipales de Tours:
- https://www.tours.fr/page-portail-ma-mairie/services-pratiques/offre-culturelle/patrimoine-histoire-archives/archives-municipales/

Archives départementales d'Indre-et-Loire (Q2860459)
- https://archives.touraine.fr/
- ["Ils, Elles...ont fait l'Histoire"](https://archives.touraine.fr/page/ils-elles-ont-fait-l-histoire)

