
## osm-update-etymology

### Rue Charles Picart Ledoux

case: orthographe

- Rue Charles Picart Ledoux
- Rue Charles Picart Le doux

Names stored in `StreetUpdate.stats` "ways_not_found".

### Rue Albert Camus

case: croissement de rue près frontière commune.

- Dans Tours [1351618](https://www.openstreetmap.org/relation/1351618)
- Dans Joué-lès-Tours [1718827](https://www.openstreetmap.org/relation/1718827)

An AssociatedStreet in CityA which have a Way
that connect to another way in AssociatedStreet in CityB
could throw this case.

Names stored in `StreetUpdate.stats` "relations_duplicate".

### Rue Honoré de Balzac

case: noeud plusieurs fois dans une relation

Doublon de noeud avec role house.

```
[379] Cyrille37\OsmStreetsEtymology\Model\Street::__set_state(array(
   'name' => 'Rue Honoré de Balzac',
   'wikidata_id' => 'Q9711',
))
[NOTICE] Cyrille37\OSM\Yapafo\OSM_Api::queryOApiQL POST https://overpass-api.de/api/interpreter
PHP Fatal error:  Uncaught Cyrille37\OSM\Yapafo\Exceptions\Exception:
duplicate member "11724575226" of type "node" in /home/cyrille/Code/dev/yapafo/src/OSM/Objects/Relation.php:270
```
