#!/usr/bin/env php
<?php
/**
 * Extract streets for a City
 * and save in CSV format
 * 
 * File columns:
 * 	- 'name': le nom de la rue tel que trouvé dans OSM
 *	- 'word': le nom de rue nettoyé pour s'approcher du "nom" à chercher dans Wikidata
 *	- 'wikidata': la valeur du tag "name:etymology:wikidata"
 *	- 'relation': l'ID de la relation (si existe)
 *	- 'a_way': l'ID de la 1ère way
 *	- 'ways_count': nombre de ways avec ce nom
 *	- 'wikidata_conflicts': si différents Wikidata ID sont trouvés
 *
 * ./app/osm-city-streets-extract.php --city=France/Tours --file=../osm-streets.csv
 * ./app/osm-city-streets-extract.php --city=76306 --file=../osm-streets.csv
 */
error_reporting(-1);

require_once(__DIR__ . '/vendor/autoload.php');

use Cyrille37\OSM\Yapafo\OSM_Api;
use Cyrille37\OSM\Yapafo\Tools\Ansi;
use Cyrille37\OSM\Yapafo\Tools\Config;
use Cyrille37\OsmStreetsEtymology\Model\City;

define('EOL', Ansi::EOL);
define('TAB', Ansi::TAB);

$shortopts = '';
$longopts  = [
	'city:',
	'file:',
];
$options = getopt($shortopts, $longopts);

Config::getInstance(__DIR__);

$console = new OsmStreetsExportConsole();

$console->hello();

$console->setCity($options['city'] ?? null);

$console->setOutputFile($options['file'] ?? null);

$console->extractStreets();

$console->export();

$console->displayStats();

class OsmStreetsExportConsole
{
	public $city;
	public $file;
	public $streets = [];
	protected $stats = [
		'api' => null,
		'streets_count' => 0,
		'relations_count' => 0,
		'ways_count' => 0,
		'relations_duplicate' => [],
	];

	public function hello()
	{
		echo EOL, TAB, Ansi::BOLD, '*** OSM streets export ***', Ansi::CLOSE, EOL, EOL;
	}

	public function setCity($city)
	{
		if (!$city) {
			echo 'Indicate the city for which the streets are exported? You can either enter a Relation_Id or a "Country/City".', EOL;
			$city = readline('City: ');
		}
		try {
			$this->city = new City($city);
		} catch (\InvalidArgumentException $ex) {
			die(Ansi::BOLD . $ex->getMessage() . Ansi::CLOSE . EOL);
		}

		echo 'Working in city ', Ansi::BOLD, $this->city->name, Ansi::CLOSE,
		' in country ', Ansi::BOLD, $this->city->country_name, Ansi::CLOSE,
		' (rel:' . $this->city->relation_id . ')',
		' ...', EOL;
	}

	public function setOutputFile($filename = null)
	{
		if (!$filename) {
			echo 'The CSV output file ?', EOL;
			$filename = readline('File name: ');
		}
		$this->file = new SplFileObject($filename, 'w');
	}

	public function extractStreets()
	{
		$qlQuery = '[timeout:10];
            rel(' . $this->city->relation_id . ');
            map_to_area ->.city;
			(
				way(area.city)[highway][highway!~"^platform$"][name];
				relation(area.city)[type~"^associatedStreet|street$"];
			);
			out tags;
        ';
		$osmapi = new OSM_Api();

		$osmapi->queryOApiQL($qlQuery);

		$relations = $osmapi->getRelations();

		foreach ($relations as $relation) {
			$name = $relation->getTag('name')->getValue();
			if (!isset($this->streets[$name])) {
				$this->streets[$name] = ['name' => $name];
				$this->stats['streets_count']++;
			} else if (isset($this->streets[$name]['relation'])) {
				$this->stats['relations_duplicate'][] = $relation->getId();
				continue;
			}
			$this->stats['relations_count']++;
			$this->streets[$name]['relation'] = ['id' => $relation->getId()];
			$t = $relation->getTag('name:etymology:wikidata');
			if ($t) {
				$this->streets[$name]['relation']['name-wikidata'] = $t->getValue();
			}
		}

		$ways = $osmapi->getWays();
		foreach ($ways as $way) {
			$name = $way->getTag('name')->getValue();
			if (!isset($this->streets[$name])) {
				$this->streets[$name] = ['name' => $name];
				$this->stats['streets_count']++;
			}
			$street = &$this->streets[$name];
			if (!isset($street['ways'])) {
				$street['ways'] = [];
			}
			$this->stats['ways_count']++;
			$w = ['id' => $way->getId()];
			$t = $way->getTag('name:etymology:wikidata');
			if ($t) {
				$w['name-wikidata'] = $t->getValue();
			}
			$street['ways'][] = $w;
		}

		$this->stats['api'] = $osmapi->getStats();
	}

	public function export()
	{
		$row = [
			'name', 'word', 'wikidata', 'relation', 'a_way', 'ways_count', 'wikidata_conflicts'
		];
		$this->file->fputcsv($row);
		foreach ($this->streets as $name => &$street) {
			$wikidata = null;
			$conflicts = [];
			if (isset($street['relation']) && isset($street['relation']['name-wikidata'])) {
				$wikidata = $street['relation']['name-wikidata'];
			}
			if (isset($street['ways'])) {
				foreach ($street['ways'] as $way) {
					if (isset($way['name-wikidata'])) {
						if (!$wikidata) {
							$wikidata = $way['name-wikidata'];
						} else if ($way['name-wikidata'] != $wikidata) {
							$conflicts[] = $way['name-wikidata'];
						}
					}
				}
			}
			$row = [
				$name,
				$this->streetNameToWords($name),
				$wikidata,
				isset($street['relation']) ? $street['relation']['id'] : '',
				isset($street['ways']) ? $street['ways'][0]['id'] : '',
				isset($street['ways']) ? count($street['ways']) : '0',
				empty($conflicts) ? '' : implode(',', array_unique($conflicts)),
			];
			$this->file->fputcsv($row);
		}
	}

	public function streetNameToWords($name)
	{
		$clean = $name;

		for ($i = 0; $i <= 1; $i++) {
			$clean = preg_replace(
				'/^'
					. '(?:Place|Rue|Avenue|Mail|Venelle|Boulevard|Allée|Impasse|Passage|Square|Rond-Point'
					. '|Route|Pont|Passerelle|Quai|Jardin|Cour|Carrefour|Carroi|Chemin|Promenade)'
					. ' (?:des |de la |de l\'|de |du |aux |d\'|l\')?'
					. '/',
				'',
				$clean
			);

			$clean = trim($clean);
			$clean = preg_replace('/^(?:n°)?\d+(?:[AB]|ter|bis)? /i', '', $clean);
			$clean = preg_replace('/^(?:a|b|t|ter|bis) /i', '', $clean);
			$clean = preg_replace('/^de la rue /i', '', $clean);
		}
		return $clean;
	}

	public function displayStats()
	{
		echo var_export($this->stats, true), EOL;
	}
}
