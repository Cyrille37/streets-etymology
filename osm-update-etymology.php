#!/usr/bin/env php
<?php
/**
 * Update some OSM Objects with their Etymoly data via a Wikidata qualifier.
 * 
 * ./osm-update-etymology.php
 * ./osm-update-etymology.php --file ../rues-de-tours.csv
 * ./osm-update-etymology.php --file ../rues-de-tours.csv --city="France/Tours"
 * 
 */

error_reporting(-1);

use Cyrille37\OSM\Yapafo\Tools\Ansi;
use Cyrille37\OSM\Yapafo\Tools\Config;
use Cyrille37\OsmStreetsEtymology\Model\City;
use Cyrille37\OsmStreetsEtymology\Model\DataInput;
use Cyrille37\OsmStreetsEtymology\Model\StreetUpdate;

/**
 * As of Composer 2.2, a new $_composer_autoload_path global variable
 * https://getcomposer.org/doc/articles/vendor-binaries.md#finding-the-composer-autoloader-from-a-binary
 */
require_once(__DIR__ . '/vendor/autoload.php');

define('EOL', Ansi::EOL);
define('TAB', Ansi::TAB);

Config::getInstance(__DIR__);

// Command line options

$shortopts = '';
$longopts  = [
	'city:',
	'file:',
	'sleep:'
];
$options = getopt($shortopts, $longopts);
//echo var_export($options, true), EOL;

// Processing

$console = new OsmUpdateEtymologieConsole();

$console->hello();

$console->loadInputFromCsv($options['file'] ?? null);

$console->setCity($options['city'] ?? null);

$console->displayStats();

$console->updateStreets($options['sleep'] ?? 5);

$console->displayStats();

class OsmUpdateEtymologieConsole
{
	public $dataInput;
	public $city;
	public $streetUpdate;

	public function __construct()
	{
		//$dic = DIContainer::getInstance();
		//$dic->add('osm_api', new OSM_Api());
	}

	public function hello()
	{
		echo EOL, TAB, Ansi::BOLD, '*** OSM update Etymology ***', Ansi::CLOSE, EOL, EOL;
	}

	public function loadInputFromCsv($filename = null)
	{
		if (!$filename) {
			echo 'We need a CSV file with streets name, wikidata id and a flag to process or ignore lines.', EOL;
			$filename = readline('File name: ');
		}
		$this->dataInput = new DataInput();
		try {
			$this->dataInput->loadFromCSVFile($filename);
		} catch (\Exception $ex) {
			die('ERROR: ' . $ex->getMessage() . EOL);
		}
	}

	public function setCity($city)
	{
		if (!$city) {
			echo 'We need a city to find the streets, you can either enter a Relation_Id or a "Country/City".', EOL;
			$city = readline('City: ');
		}
		$this->city = new City($city);

		echo 'Working in city ', Ansi::BOLD, $this->city->name, Ansi::CLOSE,
		' in country ', Ansi::BOLD, $this->city->country_name, Ansi::CLOSE,
		' (rel:' . $this->city->relation_id . ')',
		' ...', EOL;
	}

	public function updateStreets($sleepSeconds)
	{
		$this->streetUpdate = new StreetUpdate($this->city);

		$streetsCount = 0;
		foreach ($this->dataInput->getStreets() as $street) {

			$streetsCount++;
			echo Ansi::BOLD, '[', $streetsCount, ']', Ansi::CLOSE,
			' ', Ansi::CYAN, var_export($street), Ansi::CLOSE, EOL;

			$this->streetUpdate->updateNameEtymologyWikidata($street);

			if ($streetsCount % 10 == 0)
				echo 'updateStreets: ', var_export($this->streetUpdate->getStats(), true), EOL;

			if ($sleepSeconds) {
				echo Ansi::ITALIC, 'sleeping ', $sleepSeconds, ' secondes...', Ansi::CLOSE, EOL;
				sleep($sleepSeconds);
			}
		}
	}

	public function displayStats()
	{
		if ($this->dataInput)
			echo var_export($this->dataInput->getStats(), true), EOL;
		if ($this->streetUpdate)
			echo var_export($this->streetUpdate->getStats(), true), EOL;
	}
}
