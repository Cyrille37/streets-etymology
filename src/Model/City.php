<?php

namespace Cyrille37\OsmStreetsEtymology\Model;

use Cyrille37\OSM\Yapafo\OSM_Api;
use Cyrille37\OsmStreetsEtymology\Tools\DIContainer;

class City
{
    public $relation_id;
    public $name ;
    public $country_name ;
    //protected $osm_api ;

    public function __construct($cityRelidOrName)
    {
		//$this->osm_api = DIContainer::getInstance()->get('osm_api');

        if (preg_match('/^\d+$/', $cityRelidOrName)) {
            $this->loadRelationFromId($cityRelidOrName);
        } else if (preg_match('#^(.*)\/(.*)$#', $cityRelidOrName, $m)) {
            $this->loadRelationFromCountryCity($m[1], $m[2]);
        }

        if (!$this->relation_id)
            throw new \InvalidArgumentException('Failed to find a city with "' . $cityRelidOrName . '".');

    }

    protected function loadRelationFromCountryCity($country, $city)
    {
        $qlQuery = '[timeout:60];
            area[admin_level=2][name="'.$country.'"]->.country;
            (
                relation[admin_level=2][name="'.$country.'"];
                relation(area.country)[admin_level=8][name="'.$city.'"];
            );        
            out tags ;
        ';
        $osmapi = new OSM_Api();
        $osmapi->queryOApiQL($qlQuery);

        $relations = $osmapi->getRelations();
        $this->extractData( $relations, null, $city);
    }

    protected function loadRelationFromId($relation_id)
    {
        $qlQuery = '[timeout:60];
            rel('.$relation_id.');
            out tags; // get city relation & tags
            >;
            is_in->.zones;
            area.zones["admin_level"="2"][boundary="administrative"];
            rel(pivot._); // option: transform "area" to "relation"
            out tags; // get country relation & tags
        ';
        $osmapi = new OSM_Api();
        $osmapi->queryOApiQL($qlQuery);
        $relations = $osmapi->getRelations();
        $this->extractData( $relations, $relation_id, null);
    }

    protected function extractData( array $relations, $relation_id=null, $city_name=null)
    {
        if( count($relations) != 2 )
        {
            return ;
        }
        for( $i=0; $i<2; $i++)
        {
            $rel = $relations[$i];
            $name_tag_value =  $rel->getTag('name')->getValue() ;
            if( $relation_id && $rel->getId() == $relation_id )
            {
                $this->name = $name_tag_value ;
                $this->relation_id = $rel->getId() ;
            }
            else if( $city_name && $name_tag_value == $city_name)
            {
                $this->name = $name_tag_value ;
                $this->relation_id = $rel->getId() ;
            }
            else
            {
                $this->country_name =  $name_tag_value ;
            }
        }
    }
}
