<?php

namespace Cyrille37\OsmStreetsEtymology\Model;

use SplFileObject;

class DataInput
{

    protected $config = [
        'headers' => [
            'name' => -1, 'wikidata_id' => -1, 'selected' => -1,
        ],
    ];

    protected $data = [
        'stats' => [
            'lines_selected' => 0,
            'lines_skipped' => 0,
            'names_duplicate' => 0,
        ],
        'streets' => [
            // name => wikidata_id
        ],
        'wikidata_id' => [
            // wikidata_id => count
        ],
    ];

    public function getStats()
    {
        $this->data['stats']['streets_count'] = $this->data['stats']['lines_selected'] - $this->data['stats']['names_duplicate'] ;
        return $this->data['stats'];
    }

    public function getStreets()
    {

        foreach( $this->data['streets'] as $name => $wikidata_id )
        {
            yield new Street($name,$wikidata_id);
        }
    }

    public function loadFromCSVFile($filename)
    {

        $file = new SplFileObject($filename);
        $file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
        $linesCount = 0;
        $headers = $this->config['headers'];
        while (!$file->eof()) {
            $row = $file->fgetcsv();
            if (!$row)
                continue;
            if (!$linesCount) {
                $found = 0;
                foreach ($row as $pos => $str) {
                    if (isset($headers[$str])) {
                        $headers[$str] = $pos;
                        $found++;
                    }
                }
                if ($found != count($headers))
                    throw new \InvalidArgumentException('CSV headers does not match expected: ' . implode(',', $headers)) . '.';

                $linesCount++;
                continue;
            }
            $linesCount++;

            $selected = $row[$headers['selected']];
            $name = $row[$headers['name']];
            $wikidata_id = $row[$headers['wikidata_id']];

            // is line selected for processing ?
            if ($selected != 1) {
                $this->data['stats']['lines_skipped']++;
                continue;
            }
            $this->data['stats']['lines_selected']++;

            if (!$wikidata_id)
                throw new \InvalidArgumentException('Line ' . $linesCount . ' is selected but missing a wikidata_id.');

            if (isset($this->data['wikidata_id'][$wikidata_id])) {
                $this->data['wikidata_id'][$wikidata_id]++;
            } else {
                $this->data['wikidata_id'][$wikidata_id] = 1;
            }

            if (isset($this->data['streets'][$name])) {
                if ($this->data['streets'][$name] != $wikidata_id)
                    throw new \InvalidArgumentException('Line ' . $linesCount . ' has wikidata_id:' . $wikidata_id
                        . ' for street "' . $name . '" which already has wikidata_id:' . $this->data['streets'][$name] . '.');
                $this->data['stats']['names_duplicate']++;
            } else {
                $this->data['streets'][$name] = $wikidata_id;
            }
        }
    }
}
