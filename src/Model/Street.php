<?php

namespace Cyrille37\OsmStreetsEtymology\Model;

class Street
{
    public $name ;
    public $wikidata_id ;
    public function __construct($name, $wikidata_id)
    {
        $this->name = $name ;
        $this->wikidata_id = $wikidata_id;
    }
}
