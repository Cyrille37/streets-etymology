<?php

namespace Cyrille37\OsmStreetsEtymology\Model;

use Cyrille37\OSM\Yapafo\Objects\Member;
use Cyrille37\OSM\Yapafo\Objects\OSM_Object;
use Cyrille37\OSM\Yapafo\Objects\Tag;
use Cyrille37\OSM\Yapafo\Objects\Way;
use Cyrille37\OSM\Yapafo\OSM_Api;

class StreetUpdate
{
    const TAG_NameEtymologyWikidata = 'name:etymology:wikidata';

    public $city;

    protected $stats = [
        'api' => ['simulation'=>null],
        'streets_count' => 0,
        'streets_updated_count' => 0,
        'tag_' . self::TAG_NameEtymologyWikidata . '_already_count' => 0,
        'tag_' . self::TAG_NameEtymologyWikidata . '_created_count' => 0,
        'tag_' . self::TAG_NameEtymologyWikidata . '_updated_count' => 0,
        'tags_' . self::TAG_NameEtymologyWikidata . '_updated' => [],
        'relations_missing_ways' => [],
        'ways_not_found' => [],
        'relations_duplicate' => [],
        'relations_and_way_tag' => [],
    ];

    public function __construct(City $city)
    {
        $this->city = $city;
    }

    public function getStats()
    {
        return $this->stats;
    }

    public function updateNameEtymologyWikidata(Street $street)
    {
        $this->stats['streets_count']++;

        $qlQuery = '[timeout:10];
            rel(' . $this->city->relation_id . ');
            map_to_area ->.city;
            rel(area.city)[type="associatedStreet"][name="' . $street->name . '"];
            out meta ;
            way(area.city)[highway][name="' . $street->name . '"];
            out meta ;
        ';
        $osmapi = new OSM_Api();
        $this->stats['api']['simulation'] = $osmapi->isSimulation();

        $osmapi->queryOApiQL($qlQuery);

        $relations = $osmapi->getRelations();
        if (count($relations) > 1) {
            $this->stats['relations_duplicate'][] = $street->name;
            echo 'Too many associatedStreet (count:', count($relations), ') for street "', $street->name . '"', EOL;
            //throw new \RuntimeException('Too many associatedStreet (count:' . count($relations) . ') for street "' . $street->name . '"');
            //
            // Do not throw error but report and stop street processing.
            // Case: an AssociatedStreet in CityA which have a Way
            // that connect to another way in AssociatedStreet in CityB
            // could throw this case.
            //
            return;
        }

        $ways = $osmapi->getWays();
        if (count($ways) == 0) {
            $this->stats['ways_not_found'][] = $street->name;
            echo 'No way for street "', $street->name, '"', EOL;
            //throw new \RuntimeException('No way for street "' . $street->name . '"');
            return;
        }

        $objects2update = [];

        if (count($relations) == 1) {

            // We got a Relation

            $objects2update[] = $relations[0];

            // Find streets ways out of the relation
            $relation = $relations[0];
            $relations_ways = $relation->findMembersByType(OSM_Object::OBJTYPE_WAY);
            $diff = array_udiff($ways, $relations_ways, function ($a, $b) {
                if ($a instanceof Way)
                    $a = $a->getId();
                else if ($a instanceof Member)
                    $a = $a->getRef();
                else
                    throw new \RuntimeException('Failed comparaison, unknow $a type "' . get_class($a) . '"');
                if ($b instanceof Way)
                    $b = $b->getId();
                else if ($b instanceof Member)
                    $b = $b->getRef();
                else
                    throw new \RuntimeException('Failed comparaison, unknow $b type "' . get_class($a) . '"');
                return $a <=> $b;
            });
            if (count($diff) > 0) {
                //$ways2update = $ways = $osmapi->getWays();
                //echo 'Ways out of relation: ', var_export($diff, true), EOL;
                //die('Abort' . EOL);
                $this->stats['relations_missing_ways'][ $relation->getId()] = array_map(function($o){return $o->getId();},$diff);
            }

            // If some ways have a etymology tag we must process an update too
            $found = false ;
            foreach( $ways as $way )
            {
                $tag = $way->getTag(self::TAG_NameEtymologyWikidata);
                if ($tag){
                    $objects2update[] = $way ;
                }
            }
            if( $found )
            {
                $this->stats['relations_and_way_tag'][] = $relation->getId();
            }

        } else {

            // No Relation so take Ways

            $objects2update = $osmapi->getWays();
        }

        $changes = 0;

        /** @var OSM_Object $object */
        foreach ($objects2update as $object) {
            /** @var Tag $tag */
            $tag = $object->getTag(self::TAG_NameEtymologyWikidata);
            if ($tag && ($tag->getValue() == $street->wikidata_id)) {
                // Tag is up to date
                $this->stats['tag_' . self::TAG_NameEtymologyWikidata . '_already_count']++;
            } else if ($tag) {
                // Tag need update
                $this->stats['tag_' . self::TAG_NameEtymologyWikidata . '_updated_count']++;
                $this->stats['tags_' . self::TAG_NameEtymologyWikidata . '_updated'][] =
                    ['tag' => self::TAG_NameEtymologyWikidata, 'was' => $tag->getValue(), 'new' => $street->wikidata_id, 'id' => $object->getId(), 'type' => $object->getObjectType()];
                $this->stats['streets_updated_count']++;
                /*echo var_export([
                    'tag'=>$tag,
                    'street'=>$street,
                ],true),EOL;
                throw new \RuntimeException('Argh!!');*/
                $object->setTag(self::TAG_NameEtymologyWikidata, $street->wikidata_id);
                $changes++;
            } else {
                // Tag creation
                $this->stats['tag_' . self::TAG_NameEtymologyWikidata . '_created_count']++;
                $this->stats['streets_updated_count']++;
                $object->addTag(self::TAG_NameEtymologyWikidata, $street->wikidata_id);
                $changes++;
            }
        }

        if ($changes > 0) {
            $osmapi->saveChanges(
                'Update tag ' . self::TAG_NameEtymologyWikidata
                    . ' for street "' . $street->name . '"'
                    . ' in "' . $this->city->country_name . '/' . $this->city->name . '"'
                    . ' #TagNameEtymologyWikidata'
            );
        } else {
        }
    }
}
