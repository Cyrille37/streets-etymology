<?php

namespace Cyrille37\OsmStreetsEtymology\Tools;

use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class DIContainer implements ContainerInterface
{

    protected $objects;

    protected function __construct()
    {
        $this->objects = [];
    }

    public static function getInstance($dir = null)
    {
        static $instance;
        if (!$instance) {
            $instance = new self($dir);
        }
        return $instance;
    }

    public function add($id, $object)
    {
        $this->objects[$id] = $object ;
    }

    /**
     * Finds an entry of the container by its identifier and returns it.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @throws NotFoundExceptionInterface  No entry was found for **this** identifier.
     * @throws ContainerExceptionInterface Error while retrieving the entry.
     *
     * @return mixed Entry.
     */
    public function get(string $id)
    {
        if (!isset($this->objects[$id]))
            throw new NotFoundExceptionInterface('Unknow object "' . $id . '"');
        return $this->objects[$id];
    }

    /**
     * Returns true if the container can return an entry for the given identifier.
     * Returns false otherwise.
     *
     * `has($id)` returning true does not mean that `get($id)` will not throw an exception.
     * It does however mean that `get($id)` will not throw a `NotFoundExceptionInterface`.
     *
     * @param string $id Identifier of the entry to look for.
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        return isset($this->objects[$id]);
    }
}
