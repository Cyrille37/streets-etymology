<?php

/**
 * Utilisation de l'API Wikidata
 *  - https://www.wikidata.org/w/api.php
 * avec l'action `wbgetentities`
 *	- https://www.wikidata.org/w/api.php?action=help&modules=wbgetentities
 * et les `props` `labels` et `claims`
 * 
 * Par exemple:
 * https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels|claims&ids=Q19675&languages=fr&format=json
 * 
 */

namespace Cyrille37\OsmStreetsEtymology\Tools;

use Cyrille37\OSM\Yapafo\Exceptions\HttpException;
use Cyrille37\OSM\Yapafo\Tools\Config;
use Cyrille37\OSM\Yapafo\Tools\Logger;
use Psr\Log\LogLevel;

class WikidataApi
{
    const VERSION = '1.0';
    const USER_AGENT = 'https://framagit.org/Cyrille37/streets-etymology';

    const DEFAULT_LANG = 'fr';
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    protected $_options = [
        'sleep' => 1000 * 250,
        'lang' => self::DEFAULT_LANG,
        'cacheFolder' => null,
        'api_url' => 'https://www.wikidata.org/w/api.php',
        'sparql_url' => 'https://query.wikidata.org/sparql',
        'log' => [
            'logger' => null,
            'level' => LogLevel::NOTICE
        ],
    ];

    protected $_stats = array(
        'http_count' => 0,
        'http_cache_count' => 0,
        'http_bytes' => 0,
        'entities_count' => 0,
        'labels_count' => 0,
        'labels_missing' => [],
    );

    public function __construct($options = [])
    {
        $this->_options['log']['level'] = Config::get('osm_log_level', $this->_options['log']['level']);
        $this->_options['cacheFolder'] = Config::get('osm_api_cacheFolder', $this->_options['cacheFolder']);

        // Check that all options exist then override defaults
        foreach ($options as $k => $v) {
            if (!array_key_exists($k, $this->_options))
                throw new \InvalidArgumentException('Unknow option "' . $k . '"');
            $this->_options[$k] = $v;
        }

        if (isset($this->_options['log']['logger'])) {
            $this->_logger = $this->_options['log']['logger'];
        } else {
            $this->_logger = Logger::getInstance($this->_options['log']['level']);
        }
        if (!empty($this->_options['cacheFolder'])) {
            if (!is_writable($this->_options['cacheFolder'])) {
                throw new \InvalidArgumentException('Option "cacheFolder" is set but the folder "' . $this->_options['cacheFolder'] . '" is not writable.');
            }
        }
    }

    public function getLabel($wikidataId)
    {
        $this->_stats['labels_count']++;

        // https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels&ids=Q19675&languages=fr&format=json
        $queryString = '?' . http_build_query([
            'format' => 'json',
            // Remove "languages" to retrieve all languages
            // or set it like "fr|en" to get only some languages
            'languages' => $this->_options['lang'],
            'action' => 'wbgetentities',
            'props' => 'labels',
            'ids' => $wikidataId,
        ]);
        $url = $this->_options['api_url'] . $queryString;
        $result = $this->http($url, $wikidataId);
        $result = json_decode($result);
        if( $result === false || $result === null )
            throw new \RuntimeException( __METHOD__.' Failed to decode result for '.$wikidataId);
        if (isset($result->entities->{$wikidataId}->labels->{$this->_options['lang']}))
            return $result->entities->{$wikidataId}->labels->{$this->_options['lang']}->value;
        //if( isset($result->entities->{$wikidataId}->labels->en) )
        //    return $result->entities->{$wikidataId}->labels->en->value ;
        //throw new \Exception('Label not found for '.$wikidataId);
        $this->_stats['labels_missing'][] = $wikidataId;
        return $wikidataId;
    }

    public function queryEntity($wkId)
    {
        $this->_stats['entities_count']++;

        // https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels|claims&ids=Q19675&languages=fr&format=json
        $queryString = '?' . http_build_query([
            'format' => 'json', 'languages' => $this->_options['lang'],
            'action' => 'wbgetentities',
            'props' => 'labels|claims',
            'ids' => $wkId,
        ]);
        $url = $this->_options['api_url'] . $queryString;
        $result = $this->http($url, $wkId);
        if( $result === false || $result === null )
            throw new \RuntimeException(__METHOD__.' Failed to decode result for '.$wkId);
        return json_decode($result);
    }

    public function ask_is_a($wkId, array $predicats, $is_a)
    {
        // wdt:P171*/wdt:P279*
        $predicat = array_map(function($e){ return 'wdt:'.$e.'*'; },$predicats);
        $predicat = implode('/', $predicat );

        $sparql = 'ASK { wd:' . $wkId . ' ' . $predicat . ' wd:' . $is_a . ' .'
            . 'hint:Prior hint:gearing "forward".' // optimization
            . ' }';
        $url = $this->_options['sparql_url'] . '?query=' . urlencode($sparql);
        $result = $this->http($url, $wkId);
        $result = json_decode($result);
        if( $result === false || $result === null )
            throw new \RuntimeException(__METHOD__.' Failed to decode result for '.$wkId);
        return $result->boolean ;
    }

    public function &http($url, $wdId )
    {
        $headers = [
            //'Content-type: application/json',
            'Accept: application/json',
            // 'Accept: application/sparql-results+json'
        ];
        $opts = [
            'http' => [
                'method' => 'GET',
                'user_agent' => $this->getUserAgent(),
                'header' => $headers,
            ]
        ];
        $cacheFile = $this->_getCacheFilename($url, $wdId);
        $result = null;
        if ($this->_options['cacheFolder'] != null) {
            if (file_exists($cacheFile)) {
                //$this->getLogger()->notice('{method} Read from cache {url}', ['method' => __METHOD__, 'url' => $url]);
                $this->getLogger()->notice('Read from cache {url}', ['url' => $url]);
                $result = @file_get_contents($cacheFile);
                $this->_stats['http_cache_count']++;
            }
        }

        if (!$result) {
            if ($this->_stats['http_count'] > 0) {
                usleep($this->_options['sleep']);
            }
            $this->_stats['http_count']++;
            $context = stream_context_create($opts);
            $result = @file_get_contents($url, false, $context);
            if ($this->_options['cacheFolder'] != null) {
                file_put_contents($cacheFile, $result);
            }
        }

        if ($result === false || $result == null) {
            $e = error_get_last();
            if (isset($http_response_header)) {
                $ex = new HttpException($http_response_header);
            } else {
                $ex = new HttpException($e['message']);
            }
            if ($ex->getMessage() != 'HTTP/1.1 200 OK')
                throw $ex;
        }

        $this->_stats['http_bytes'] += strlen($result);

        return $result;
    }

    public function getUserAgent()
    {
        $userAgent = self::USER_AGENT . ' v' . self::VERSION;
        return $userAgent;
    }

    protected function _getCacheFilename($url, $wdId)
    {
        if (empty($this->_options['cacheFolder']))
            return null;
        return $this->_options['cacheFolder'] .
            DIRECTORY_SEPARATOR . __CLASS__
            . '_cache-'.$wdId. '-' . sha1($url);
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->_logger;
    }

    public function isDebug()
    {
        return ($this->_options['log']['level'] == LogLevel::DEBUG);
    }

    public function getStats()
    {
        return $this->_stats;
    }
}
