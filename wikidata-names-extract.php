#!/usr/bin/env php
<?php
/**
 * Pour chaque ligne d'un fichier avec colonnes "name" et "wikidata",
 * retrouve sur Wikidata les déclarations correspondantes.
 * 
 * 
 */
error_reporting(-1);

use Cyrille37\OSM\Yapafo\Tools\Ansi;
use Cyrille37\OSM\Yapafo\Tools\Config;
use Cyrille37\OsmStreetsEtymology\Tools\WikidataApi;

require_once(__DIR__ . '/vendor/autoload.php');

define('EOL', Ansi::EOL);
define('TAB', Ansi::TAB);

$shortopts = '';
$longopts  = [
	'file:', 'lang:', 'file_out:',
];
$options = getopt($shortopts, $longopts);

Config::getInstance(__DIR__);

$console = new WikidataNamesExtractConsole();
$console->hello();
$console->loadInputFromCsv($options['file'] ?? null);
$console->retrieveClaims($options['lang'] ?? 'fr');

$console->exportClaims($options['file_out'] ?? null);
$console->displayStats();

class WikidataNamesExtractConsole
{
	protected $file_in;
	protected $headers = [
		'name' => -1, 'wikidata' => -1
	];
	protected $streets = [];
	/**
	 * @var WikidataApi
	 */
	protected $api;
	protected $stats = [
		'lines_count' => 0,
		'orphans_count' => 0,
		'wikidata_count' => 0,
		'wikidataSeen_count' => 0,
		'labels_missing' => [],
	];

	public function hello()
	{
		echo EOL, TAB, Ansi::BOLD, '*** Retrieve word’s etymology ***', Ansi::CLOSE, EOL, EOL;
	}

	public function loadInputFromCsv($filename = null)
	{
		if (!$filename) {
			echo 'We need a CSV file with streets name, wikidata.', EOL;
			$filename = readline('File name: ');
		}
		$this->file_in = $filename;
		$file = new SplFileObject($filename);
		$file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
		$linesCount = 0;
		while (!$file->eof()) {
			$row = $file->fgetcsv();
			if (!$row)
				continue;
			if (!$linesCount) {
				$found = 0;
				foreach ($row as $pos => $str) {
					if (isset($this->headers[$str])) {
						$this->headers[$str] = $pos;
						$found++;
					}
				}
				if ($found != count(array_keys($this->headers)))
					throw new \InvalidArgumentException('CSV headers does not match expected: ' . implode(',', array_keys($this->headers)) . '.');

				$linesCount++;
				continue;
			}
			$linesCount++;

			// Pour l'instant on ne prend que le 1er :(
			$wikidata = explode(';', $row[$this->headers['wikidata']]);

			$this->streets[] = [
				'name' => $row[$this->headers['name']],
				'wikidata' => $wikidata[0],
				'label' => null,
				'claims' => [],
			];
		}
		$this->stats['lines_count'] = $linesCount - 1;
	}

	/**
	 * Utilise l'API Wikidata pour retrouver les "claims" et les "labels" de chaque élément WD associé aux rues
	 * 
	 * Exemple de requête :
	 * https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels|claims&ids=Q19675&languages=fr&format=json
	 */
	public function retrieveClaims($lang)
	{
		$wikidataSeen = [];

		$this->api = new WikidataApi(['lang' => $lang]);
		foreach ($this->streets as &$street) {
			if (empty($street['wikidata'])) {
				$this->stats['orphans_count']++;
				continue;
			}
			$wikidataId = $street['wikidata'];
			if (isset($wikidataSeen[$wikidataId])) {
				$this->stats['wikidataSeen_count']++;
				continue;
			}
			$wikidataSeen[$wikidataId] = true;

			$this->stats['wikidata_count']++;
			echo $wikidataId, EOL;

			$result = $this->api->queryEntity($wikidataId);
			$entity = $result->entities->{$wikidataId};

			if (isset($entity->labels->{$lang}))
				$street['label'] = $entity->labels->{$lang}->value;
			else {
				$this->stats['labels_missing'][] = $wikidataId;
				$street['label'] = $wikidataId;
			}

			foreach ($this->properties as $prop => $propData) {
				if (isset($entity->claims->{$prop})) {
					$this->processStreetProperties($prop, $entity, $street);
				}
			}
		}
	}

	public $properties = [
		'P31' => [
			'label' => 'nature de l’élément',
			'count' => 0,
			'claims_ignore' => [],
			'claims' => [],
			'classes' => [
				'Q5' => 'humain',

				'Q5113' => 'oiseau',
				'Q729' => 'animal',
				'Q7377' => 'animal', // mammifères
				'Q756' => 'plante',
				'Q10884' => 'arbre',

				'Q20011319' => 'plante', // partie d'une plante

				'Q481289' => 'lieux', // résidence officielle
				'Q16970' => 'lieux', // P31/Q16970 - église
				'Q160742' => 'lieux', // abbaye
				'Q15823129' => 'lieux', // P31/Q15823129 - chartreuse
				'Q627236' => 'lieux', // cité ouvrière
				'Q64578911' => 'lieux', // ancien hôpital
				'Q15848826' => 'lieux', // palais urbain
				'Q1248784' => 'lieux', // aéroport
				'Q1365179' => 'lieux', // P31/Q1365179 - hôtel particulier
				'Q23442' => 'lieux', // P31/Q23442 - île
				'Q123705' => 'lieux', // P31/Q123705 - quartier
				'Q728937' => 'lieux', // - ligne de chemin de fer
				'Q23413' =>  'lieux', // château fort
				'Q751876' => 'lieux', // château
				'Q1434274' => 'lieux', // lieu-dit

				'Q484170' => 'ville', // commune française
				'Q515' => 'ville', // ville
				'Q15273785' => 'ville', // commune belge avec le titre de ville
				'Q1549591' => 'ville', // grande ville
				'Q2785216' => 'ville', // P31/Q2785216 - section de commune
				'Q3502482' => 'région', // région culturelle
				'Q1970725' => 'région', // P31/Q1970725 - région naturelle
				'Q36784' => 'région', // P31/Q36784 - région de France
				'Q572995' => 'région', // P31/Q572995 - région naturelle de France
				'Q6465' => 'région', // département français
				'Q35657' => 'région', // État des États-Unis
				'Q16110' => 'région', // P31/Q16110 - région de l'Italie
				'Q83116' => 'région', // P31/Q83116 - province de Belgique
				'Q19953632' => 'région', // P31/Q19953632 - entité territoriale disparue
				'Q5107' => 'région', // P31/Q5107 - continent
				'Q6256' => 'pays', // pays
				'Q3024240' => 'pays', // P31/Q3024240 - État historique
				'Q133156' => 'colonie', // P31/Q133156 - colonie

				'Q28640' => 'profession', // profession
				'Q88789639' => 'profession', // profession artistique

				'Q82955' => 'politique', // personnalité politique

				'Q8142' => 'concept', // monnaie
				'Q268592' => 'concept', // P31/Q268592 - branche économique
				'Q12737077' => 'concept', // P31/Q12737077 - activité
				'Q840396' => 'concept', // idéal
				'Q8161' => 'concept', // P31/Q8161 - impôt
				'Q2558684' => 'concept', // P31/Q2558684 - journée internationale
				'Q1197685' => 'concept', // P31/Q1197685 - jour férié
				'Q367293' => 'concept', // P31/Q367293 - convention
				'Q54445847' => 'concept', // P31/Q54445847 type d’interaction économique

				'Q188055' => 'bataille', // siège militaire
				'Q178561' => 'bataille', // bataille

				'Q47064' =>'militaire', // militaire
				'Q3455196' => 'militaire', // régiment d'infanterie français
				'Q176799' => 'militaire', // unité militaire
				'Q101370382' => 'militaire', // P31/Q101370382 - association d'anciens combattants
				'Q3199915' => 'massacre', // massacre

				'Q2061186' => 'religieux', // ordre religieux
				'Q22813674' => 'religieux', // groupe de personnages de la Bible
				'Q732948' => 'religieux', // société de vie apostolique
				'Q28653' => 'religieux', // ordre mendiant
				'Q63187345' => 'religieux', // occupation religieuse

				'Q13417114' => 'famille', // maison noble
				'Q2006518' => 'famille', // famille royale
				'Q14073567' => 'famille', // duo familial
				'Q4830453' => 'famille', //  - firme
				'Q43229' => 'organisation', // organisation

				'Q4022' => 'cours d’eau', // rivière
				'Q12284' => 'cours d’eau', // canal
				'Q23397' => 'cours d’eau', // P31/Q23397 - lac
				'Q573344' => 'cours d’eau', // P31/Q573344 - fleuve
			],
			'ask' => [
				'Q16521' => [ // taxon
					'props' => [
						'P171', // taxon supérieur
						'P279', // sous-classe de
					],
					'is_a' => [
						'Q5113', // oiseau
						'Q756', // plante
						'Q10884', // arbre
						'Q7377', // mammifères
					],
				],
				'Q55983715' => [ // groupe d'organismes connu par un nom commun
					'props' => [
						'P171', // taxon supérieur
						'P279', // sous-classe de
					],
					'is_a' => [
						'Q5113', // oiseau
						'Q756', // plante
						'Q10884', // arbre
						'Q729', // animal
					],
				],
				'Q5' => [ // être humain
					'props' => [
						'P106', // occupation
						'P39', // fonction
					],
					'is_a' => [
						'Q47064', // militaire
						'Q82955', // personnalité politique
						'Q63187345', // occupation religieuse
					],

				],
			],
			/*
			Merle Q934041
				nom vernaculaire Q502895
					de Property:P642
						oiseau Q5113
			*/
			'vernaculaire' => [
				'Q5113'
			],
		],
		'P279' => [
			'label' => 'sous-classe de',
			'count' => 0,
			'claims_ignore' => [
				'Q1190554', // occurrence
			],
			'claims' => [],
			'classes' => [

				'Q5113' => 'oiseau',
				'Q729' => 'animal',
				'Q756' => 'plante',
				'Q10884' => 'arbre',

				/*
				'Q20011319' => 'plante', // partie d'une plante
				'Q886167' => 'plante', // P279/Q886167 - fleurs
				'Q26817508' => 'plante', // cultivar de rosier

				'Q25341' => 'animal', // Passeriformes, ordre d'oiseaux
				'Q57814795' => 'animal', // mammifère domestique
				*/

				'Q837556' => 'massacre', // migration forcée
				'Q186361' => 'massacre', // châtiment

				'Q3375127' => 'concept', // Perche (outil)
				'Q37654' => 'concept', // marché (institution qui permet le commerce)

				'Q2385804' => 'organisation', // P279/Q2385804 - institution de formation

				'Q3947' => 'lieux', // maison
				'Q131596' => 'lieux', // P279/Q131596 - ferme
				'Q18397999' => 'lieux', // P279/Q18397999 - forêt de feuillus

				'Q11900058' => 'explorateur',
				'Q22813352' => 'explorateur',
				'Q2873500' => 'explorateur',

				'Q63187345' => 'religieux',
			],
		],
		'P106' => [
			'label' => 'occupation',
			'count' => 0,
			'claims_ignore' => [],
			'claims' => [],
			'classes' => [
				'Q250867' =>'religieux',
				'Q42603'=>'religieux',
				'Q11900058' => 'explorateur',
				'Q22813352' => 'explorateur',
				'Q2873500' => 'explorateur',
				'Q189290' => 'militaire',
				'Q151197' => 'militaire',
				'Q12806039' => 'militaire',
			],
		],
		'P21' => [
			'label' => 'sexe ou genre',
			'count' => 0,
			'claims_ignore' => [],
			'claims' => [],
			'classes' => [
				'Q6581097' => 'masculin',
				'Q6581072' => 'féminin',
			],
		],
	];

	/**
	 * 
	 */
	protected function processStreetProperties($prop, $entity, &$street)
	{
		$this->properties[$prop]['count']++;
		$claims = $entity->claims->{$prop};
		// mainly only one $claim for a $prop
		foreach ($claims as $claim) {

			$wId = $claim->mainsnak->datavalue->value->id;

			$this->updateStreetProperty($wId, $prop, $street);

			// faut-il rechercher un ascendant du $claim ?
			if (isset($this->properties[$prop]['ask']) && isset($this->properties[$prop]['ask'][$wId])) {
				/* 'ask' => [
					'Q16521' => [ // taxon
						'prop' => 'P171', // taxon supérieur
						'is_a' => ['Q5113'],	// oiseau
					],
				],*/
				$wIdAscendant = null;
				$ask = $this->properties[$prop]['ask'][$wId];
				foreach ($ask['is_a'] as $ascendant) {
					$is_a = $this->api->ask_is_a($entity->id, $ask['props'], $ascendant);
					//echo 'is_a ',($is_a?'true':'false'),"\n";
					if ($is_a) {
						$wIdAscendant = $ascendant;
						break;
					}
				}

				if ($wIdAscendant) {
					$this->updateStreetProperty($wIdAscendant, $prop, $street);
				}
			}

			// Traitement particulier du nom vernaculaire Q502895 qualifié avec la propriété "de" Property:P642
			if ($wId == 'Q502895' && isset($claim->qualifiers) && isset($this->properties[$prop]['vernaculaire'])) {
				foreach ($claim->qualifiers as $pId => $qualifiers) {
					if ($pId != 'P642')
						continue;
					foreach ($qualifiers as $qualifier) {
						$qId = $qualifier->datavalue->value->id;
						if (in_array($qId, $this->properties[$prop]['vernaculaire'])) {
							$this->updateStreetProperty($qId, $prop, $street);
						}
					}
				}
			}
		}
	}

	protected function updateStreetProperty($wId, $prop, &$street)
	{
		if (isset($this->properties[$prop]['claims_ignore']) && in_array($wId, $this->properties[$prop]['claims_ignore']))
			return;

		if (!isset($this->properties[$prop]['claims'][$wId])) {
			$label =  $this->api->getLabel($wId);
			$this->properties[$prop]['claims'][$wId] = [
				'label' => $label,
				'count' => 1,
			];
		} else {
			$label = $this->properties[$prop]['claims'][$wId]['label'];
			$this->properties[$prop]['claims'][$wId]['count']++;
		}
		if (!isset($street['claims'][$prop]))
			$street['claims'][$prop] = [];
		$street['claims'][$prop][$wId] = $label;
	}

	public function exportClaims($file_out)
	{
		//echo var_export($this->streets,true),"\n";

		if (!$file_out)
			$file_out = $this->file_in . '-claims.csv';
		$file = new SplFileObject($file_out, 'w');

		// Prepare classes
		$classes = [];
		$classIndex = [];
		$idx = 0;
		foreach ($this->properties as $k => $prop) {
			if (!isset($prop['classes']))
				continue;
			foreach ($prop['classes'] as $value => $label) {
				if (!isset($classIndex[$label])) {
					$classes[$idx] = $label;
					$classIndex[$label] = $idx;
					$idx++;
				}
			}
		}

		// Create headers

		$row = array_merge(['name', 'label', 'wikidata', 'classe'], $classes);

		foreach ($this->properties as $k => $prop) {
			foreach ($prop['claims'] as $wId => $claim) {
				//$row[] = $prop['label'].' - '.$claim['label'] ;
				$row[] = $k . '/' . $wId . ' - ' . $claim['label'];
			}
		}
		$file->fputcsv($row);

		// Create the row

		foreach ($this->streets as $k => $street) {
			if (empty($street['label'])) {
				continue;
			}

			$cRow = [];
			foreach ($classes as $class) {
				$cRow[] = '';
			}

			$classFound = false;
			$wRow = [];
			foreach ($this->properties as $propId => $prop) {
				if (!isset($street['claims'][$propId])) {
					foreach ($prop['claims'] as $wId => $claim)
						$wRow[] = '';
					continue;
				}
				foreach ($prop['claims'] as $wId => $claim) {
					if (!isset($street['claims'][$propId][$wId])) {
						$wRow[] = '';
						continue;
					}
					$wRow[] = '1';
					if (isset($prop['classes'])) {
						if (isset($prop['classes'][$wId])) {
							$cRow[$classIndex[$prop['classes'][$wId]]] = '1';
							$classFound = true;
						}
					}
				}
			}
			$row = [$street['name'], $street['label'], $street['wikidata'], ($classFound ? '1' : '')];
			$row = array_merge($row, $cRow, $wRow);
			$file->fputcsv($row);
		}
		//$file->fclose();
		$file = null;

		$this->csv2ods( $file_out );
	}

	protected function csv2ods( $file_csv )
	{
		$reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
		$spreadsheet = $reader->load($file_csv);

		$sheet = $spreadsheet->getActiveSheet();

		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();
		$sheet->setAutoFilter('A1:'.$highestColumn.'1');
		// Ajoute une ligne pour y ajouter des sommes de colonnes
		$sheet->insertNewRowBefore(1, 1);
		foreach( ['E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA'] as $c )
		{
			$sheet->setCellValue($c.'1','=SUM('.$c.'3:'.$c.$highestRow.')');
		}
		$sheet->freezePane('C3');

		// @fixme largeur de colonnes ne fonctionne pas
		// est-ce un bug de version ?
		//$sheet->getProtection()->setFormatColumns(false);
		$sheet->getColumnDimension('A')->setAutoSize(false)->setWidth(4.0, 'cm');
		$sheet->getColumnDimension('B')->setAutoSize(false)->setWidth(4.0, 'cm');

		$file_out = $file_csv.'.ods' ;
		$writer = new \PhpOffice\PhpSpreadsheet\Writer\Ods($spreadsheet);
		$writer->save( $file_out );
	}

	public function displayStats()
	{
		//echo var_export($this->streets, true), EOL;
		echo var_export($this->properties, true), EOL;
		echo var_export($this->stats, true), EOL;
		if ($this->api)
			echo var_export($this->api->getStats(), true), EOL;
	}
}
